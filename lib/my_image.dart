import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class MyImage extends StatelessWidget {
  final XFile xfile;

  MyImage({Key key, @required this.xfile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Uint8List>(
        future: xfile.readAsBytes(),
        builder: (context, bytes) {
          return Scaffold(
              appBar: AppBar(
                title: Text("Go back"),
              ),
              body: Center(
                  child: Column(
                children: [
                  new Image.memory(bytes.data),
                  Text(
                    xfile.path,
                    style: TextStyle(color: Colors.blue, fontSize: 10),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Go back!'),
                  ),
                ],
              )));
        });
  }
}
