import 'package:fcamera/camera_widget.dart';
import 'package:fcamera/widget_three.dart';
import 'package:fcamera/widget_two.dart';
import 'package:flutter/material.dart';

import 'my_image.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Camera test'),
      routes: {
        // '/img': (context) => MyImage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentTab = 0;

  final List<Widget> _tabs = [
    new CameraWidget(),
    new WidgetTwo(),
    new WidgetThree()
  ];

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: PageView(
        controller: pageController,
        onPageChanged: (index) {
          pageChanged(index);
        },
        children: _tabs,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentTab,
        onTap: (i) {
          bottomTapped(i);
        },
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.device_hub),
            label: "One",
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.theater_comedy),
            label: "Two",
          ),
          BottomNavigationBarItem(icon: Icon(Icons.palette), label: "Three")
        ],
      ),
    );
  }

  void pageChanged(int index) {
    setState(() {
      _currentTab = index;
    });
  }

  void bottomTapped(int index) {
    setState(() {
      _currentTab = index;
      pageController.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }
}
