import 'package:flutter/material.dart';

class WidgetOne extends StatefulWidget {
  @override
  _WidgetOne createState() => _WidgetOne();
}

class _WidgetOne extends State<WidgetOne> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        children: [
          Hero(
            tag: 'one',
            child: Text(
              'one',
              style: TextStyle(color: Colors.blue, fontSize: 40),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              // Navigator.pop(context);
            },
            child: Text('PressME!'),
          ),
        ],
      ),
    ));
  }
}
