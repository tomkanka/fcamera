import 'package:camera/camera.dart';
import 'package:fcamera/my_image.dart';
import 'package:flutter/material.dart';

class CameraWidget extends StatefulWidget {
  @override
  _CameraWidget createState() => _CameraWidget();
}

class _CameraWidget extends State<CameraWidget> {
  Future<List<CameraDescription>> cameras;

  CameraController controller;

  @override
  void initState() {
    super.initState();
    cameras = getCameras();

    cameras.then((_) {
      controller = CameraController(_[0], ResolutionPreset.medium);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (controller == null) {
      return Container();
    }
    if (!controller.value.isInitialized) {
      return Container();
    }
    return Scaffold(
        body: Center(
      child: Column(
        children: [
          Text(
            'CAMERA',
            style: TextStyle(color: Colors.blue, fontSize: 40),
          ),
          CameraPreview(controller),
          ElevatedButton(
            onPressed: () {
              takePicture();
            },
            child: Text('PressME!'),
          ),
        ],
      ),
    ));
  }

  Future<List<CameraDescription>> getCameras() async {
    var c = await availableCameras();
    return c;
  }

  void takePicture() {
    Future<XFile> pic = controller.takePicture();
    pic.then((_) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MyImage(
                    xfile: _,
                  )));
    });
  }
}
