import 'package:flutter/material.dart';

class WidgetThree extends StatefulWidget {
  @override
  _WidgetThree createState() => _WidgetThree();
}

class _WidgetThree extends State<WidgetThree> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Go back"),
        ),
        body: Center(
          child: Column(
            children: [
              Hero(
                tag: 'three',
                child: Text(
                  'THREE',
                  style: TextStyle(color: Colors.blue, fontSize: 40),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  // Navigator.pop(context);
                },
                child: Text('Go back!'),
              ),
            ],
          ),
        ));
  }
}
