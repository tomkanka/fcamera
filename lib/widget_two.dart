import 'package:flutter/material.dart';

class WidgetTwo extends StatefulWidget {
  @override
  _WidgetTwo createState() => _WidgetTwo();
}

class _WidgetTwo extends State<WidgetTwo> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Go back"),
        ),
        body: Center(
          child: Column(
            children: [
              Hero(
                tag: 'two',
                child: Text(
                  'TWO',
                  style: TextStyle(color: Colors.blue, fontSize: 40),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  // Navigator.pop(context);
                },
                child: Text('Go back!'),
              ),
            ],
          ),
        ));
  }
}
